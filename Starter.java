package main;
import java.util.Arrays;

/*  ������� ��� ���������� � ���������� � ������� �������� � ��������� ��������� ��������: �����, ��������������� ������ �������� ������ � ������� ������������������ ��������
    �����, ��������������� ������ ���������� �������� (������� � 89 ...) � ������� ����������� ��������
    �����, ��������� �� ��������� ���� ��������� ���� ������ ���������� �������� ��� ������ ��������� ��������
    �����, ��������� �� ��������� ������� ��������� ���� ������ ���������� �������� � ������� ������������� ��������
    ���������� ����������� �� ������� �������� ������� �� ������� �� 26 ������������ �� ������� ������ �������� � ������� ������
    ������ ����������� �������� � ������� ��������, ����� �������� ������������� ���������� ����� ��������
*/
public class Starter {
    public static void main(String[] args) {
        //����
        int studentBook = 0x9A4F;
        long phone = 8_952_095_99_40L;
        byte phoneBinary = 0b1011110;
        short phoneOct = 013552;

        //������
        int studentNumber;
        String stringStudentNumber = String.valueOf(studentBook);// �������� ����� ������� � ������� ������
        char[] chStudentNumber = stringStudentNumber.toCharArray();
        //�������� ���� �������, ������ ����������� ������ ��������, �� ���� ��������� 2 �����
        chStudentNumber = Arrays.copyOfRange(chStudentNumber, chStudentNumber.length -2, chStudentNumber.length);
        stringStudentNumber = String.valueOf(chStudentNumber);
        studentNumber = Integer.parseInt(stringStudentNumber);

        int del = (studentNumber+1) % 26 + 1; //�������
        char symbol = (char) (del + 64); // �� � ��� � ������� ascii ���� ��� ������� 65, �� ��������� ��� ��������������
        System.out.println(del);
        System.out.println(symbol);

    }
}
